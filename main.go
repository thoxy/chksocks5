package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/cheggaaa/pb"
	"github.com/galdor/go-cmdline"
	au "github.com/logrusorgru/aurora"
	"golang.org/x/net/proxy"
)

type prox struct {
	id    int
	Proxy string
	Type  string
	Work  bool
	Time  float64
}

func appendStringToFile(path, text string) error {
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.WriteString(text + "\n")
	if err != nil {
		return err
	}
	return nil
}

func makeR(proxie []string, URL string, Contain string, ch chan<- prox, jobs <-chan int, touT int) {
	p := proxie
	for j := range jobs {
		if p[j] != "" {
			time.Sleep(1 * time.Nanosecond)
			start := time.Now()
			dialer, err2 := proxy.SOCKS5("tcp", p[j], nil, proxy.Direct)
			if err2 == nil {
				httpTransport := &http.Transport{}
				httpClient := &http.Client{Transport: httpTransport, Timeout: time.Second * time.Duration(touT)}
				httpTransport.Dial = dialer.Dial
				req, err1 := http.NewRequest("GET", URL, nil)
				if err1 == nil {
					resp, err := httpClient.Do(req)
					if err == nil {
						secs := time.Since(start).Seconds()
						defer resp.Body.Close()
						b, _ := ioutil.ReadAll(resp.Body)
						if strings.Contains(string(b), Contain) {
							ch <- prox{j, p[j], "SOCKS5", true, secs}
						} else {
							ch <- prox{j, p[j], "SOCKS5", false, 0}
						}
					} else {
						ch <- prox{j, p[j], "SOCKS5", false, 0}
					}
				} else {
					ch <- prox{j, p[j], "SOCKS5", false, 0}
				}
			} else {
				ch <- prox{j, p[j], "SOCKS5", false, 0}
			}
		}
	}
	return
}

func readLineByLine(path string) []string { //Read all line of the text file and return a slice of string ip:port
	var url []string
	r, _ := regexp.Compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\:\\d{1,5})")

	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if r.MatchString(scanner.Text()) == true {
			for _, t := range r.FindAllStringSubmatch(scanner.Text(), -1) {
				url = append(url, t[1])
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return url
}

func removeDuplicatesUnordered(elements []string) []string { //Remove duplicate string in slice of string
	encountered := map[string]bool{}
	// Create a map of all unique elements.
	for v := range elements {
		encountered[elements[v]] = true
	}
	// Place all keys from the map into a slice.
	result := []string{}
	for key := range encountered {
		result = append(result, key)
	}
	return result
}

func createFile(path string) {
	// detect if file exists
	var _, err = os.Stat(path)
	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if err != nil {
			return
		}
		defer file.Close()
	} else {
		os.Remove(path)
		var _, err = os.Create(path)
		if err != nil {
			return
		}
	}
}

func wait3sec() {
	fmt.Println(au.Bold(au.Red("Start checking in 3 second!")))
	time.Sleep(time.Second * 1)
	fmt.Printf("\033[2K")
	fmt.Printf("\033[A")
	fmt.Println(au.Bold(au.Brown("Start checking in 2 second!")))
	time.Sleep(time.Second * 1)
	fmt.Printf("\033[2K")
	fmt.Printf("\033[A")
	fmt.Println(au.Bold(au.Green("Start checking in 1 second!")))
	time.Sleep(time.Second * 1)
	fmt.Printf("\033[2K")
	fmt.Printf("\033[A")
	fmt.Printf("\033[4;37m")
	log.Print(au.Bold(">¤"), au.Bold(au.Green("  Lets Go!\n")))
}

func menu(list string, threads string, Site string, Contain string, timeOut string, verb bool, errs bool) {

	var dirslash string
	if runtime.GOOS == "windows" {
		dirslash = "\\"
	} else {
		dirslash = "/"
	}

	runtime.GOMAXPROCS(4)
	readF := readLineByLine(list)
	RemoveDupe := removeDuplicatesUnordered(readF)
	length := len(readF)

	to, _ := strconv.Atoi(timeOut)

	log.Print(au.Bold(au.Green(">¤  ========================     Program Started     =====\n")))
	fmt.Println(au.Bold(au.Black("▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓")))
	fmt.Println(au.Bold(au.Brown("Options : ")))
	fmt.Println(au.Bold(au.Brown("══════════════════════════════════════════════════════════════════════════════")))
	fmt.Println("Used list file :", au.Bold(list))
	fmt.Println("Number of thread used : ", au.Bold(threads))
	fmt.Println("Site used to check : ", au.Bold(Site))
	fmt.Println("String used to check if proxies work : ", au.Bold(Contain))
	fmt.Println("Timeout of checker : ", au.Bold(timeOut), "s")
	fmt.Println(au.Bold(au.Brown("══════════════════════════════════════════════════════════════════════════════")))
	fmt.Println(au.Bold(au.Blue("List Info :")))
	fmt.Println(au.Bold(au.Blue("══════════════════════════════════════════════════════════════════════════════")))
	fmt.Println("Original file length : ", au.Bold(length))
	fmt.Println("Dupe : ", au.Bold(length-len(RemoveDupe)))
	fmt.Println("Without dupe length : ", au.Bold(len(RemoveDupe)))
	t := time.Now()
	Name := "Checked_" + t.Format("2006-01-02_150405") + ".txt"
	pt := filepath.Dir(list)
	fmt.Println("Append result to : ", au.Bold(pt+dirslash+Name))
	createFile(pt + dirslash + Name)
	log.Println(au.Bold(au.Green(" >¤  File created successful!")))
	fmt.Println(au.Bold(au.Blue("══════════════════════════════════════════════════════════════════════════════")))
	fmt.Println(au.Bold(au.Black("▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓\n")))
	wait3sec()

	ch := make(chan prox, length)
	jobs := make(chan int, length)

	th, _ := strconv.Atoi(threads)
	for w := 0; w < th; w++ {
		go makeR(readF, Site, Contain, ch, jobs, to)
	}
	for j := 0; j <= length; j++ {
		if j != length {
			jobs <- j
		} else {
			break
		}
	}
	close(jobs)

	var bar *pb.ProgressBar
	count := length
	if verb == false {
		bar = pb.New(count)
		bar.Format("║░█ ║")
		bar.SetWidth(78)
		bar.SetMaxWidth(78)
		bar.ShowPercent = true
		bar.ShowBar = true
		bar.ShowCounters = true
		bar.ShowTimeLeft = true
		bar.ShowSpeed = true
		bar.ShowBar = true
		bar.Start()
	}

	for i := 0; i < length; i++ {
		if i != length {
			cc := <-ch
			if cc.Work == true && cc.Time != 0 {
				if verb == true {
					if cc.Time < 5 {
						log.Print(au.Bold(au.Blue(">¤")), au.Bold("Line: "), cc.id, au.Bold(au.Blue(" >¤")), au.Bold("Proxy: "), cc.Proxy, au.Bold(au.Blue(" >¤")), au.Bold("Time: "), au.Green(cc.Time), au.Green("s "))
					} else if cc.Time > 5 && cc.Time < 10 {
						log.Print(au.Bold(au.Blue(">¤")), au.Bold("Line: "), cc.id, au.Bold(au.Blue(" >¤")), au.Bold("Proxy: "), cc.Proxy, au.Bold(au.Blue(" >¤")), au.Bold("Time: "), au.Brown(cc.Time), au.Brown("s "))
					} else if cc.Time > 10 {
						log.Print(au.Bold(au.Blue(">¤")), au.Bold("Line: "), cc.id, au.Bold(au.Blue(" >¤")), au.Bold("Proxy: "), cc.Proxy, au.Bold(au.Blue(" >¤")), au.Bold("Time: "), au.Red(cc.Time), au.Red("s "))
					}
				}
				appendStringToFile(pt+dirslash+Name, cc.Proxy)
			} else {
				if verb == true && errs == true {
					log.Print(au.Bold(au.Brown(">¤")), au.Bold("Line: "), cc.id, au.Bold(au.Brown(" >¤")), au.Bold("Proxy: "), cc.Proxy, au.Bold(au.Brown(" >¤ ")), au.Bold(au.Red("Error = Proxy not work!")))
				}
			}
			if verb == false {
				bar.Increment()
			}
		}
	}
	if verb == false {
		bar.FinishPrint("\nFinished!")
	}
	fmt.Println(au.Brown("\n▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓"))
	fmt.Println(au.Bold(au.Green("Result : \n══════════════════════════════════════════════════════════════════════════════")))
	fmt.Println("Total work time : ", au.Bold(time.Since(t)))
	fmt.Println("Number of proxies checked : ", au.Bold(len(RemoveDupe)))
	fmt.Println("Number of proxies not work : ", au.Bold(au.Red(len(RemoveDupe)-len(readLineByLine(pt+dirslash+Name)))))
	fmt.Println("Number of proxies work : ", au.Bold(au.Green(len(readLineByLine(pt+dirslash+Name)))))
	fmt.Println("Result file : ", au.Bold(pt+dirslash+Name))
	fmt.Println(au.Bold(au.Green("══════════════════════════════════════════════════════════════════════════════")))
	fmt.Println(au.Brown("▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓"))
	pause(true)
}

func init() {
	if runtime.GOOS == "windows" {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	} else {
		c := exec.Command("clear")
		c.Stdout = os.Stdout
		c.Run()
	}

	fmt.Print(
		au.Red("\n   ██████╗██╗  ██╗██╗  ██╗███████╗ ██████╗  ██████╗██╗  ██╗███████╗███████╗\n"),
		au.Green(" ██╔════╝██║  ██║██║ ██╔╝██╔════╝██╔═══██╗██╔════╝██║ ██╔╝██╔════╝██╔════╝\n"),
		au.Brown(" ██║     ███████║█████╔╝ ███████╗██║   ██║██║     █████╔╝ ███████╗███████╗\n"),
		au.Blue(" ██║     ██╔══██║██╔═██╗ ╚════██║██║   ██║██║     ██╔═██╗ ╚════██║╚════██║\n"),
		au.Magenta(" ╚██████╗██║  ██║██║  ██╗███████║╚██████╔╝╚██████╗██║  ██╗███████║███████║\n"),
		au.Magenta("  ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝ ╚═════╝  ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝\n"),
		au.Bold(au.Black("                                                                      Thoxy\n\n")),
	)
}

func main() {

	cmdline := cmdline.New()

	cmdline.AddOption("i", "input", "file.txt", "the input proxies list file")
	cmdline.AddOption("t", "timeout", "time", "timeout in second")
	cmdline.AddOption("x", "threads", "number", "number of threads")
	cmdline.AddOption("u", "url", "site", "site used to check proxies server")
	cmdline.AddOption("c", "contain", "string", "check on site body if the string is contained")
	cmdline.AddFlag("v", "verbose", "log more information but not use the progress bar")
	cmdline.AddFlag("e", "error", "print proxies that do not work (need verbose)")

	cmdline.SetOptionDefault("threads", "100")
	cmdline.SetOptionDefault("timeout", "15")
	cmdline.SetOptionDefault("url", "http://azenv.net/")
	cmdline.SetOptionDefault("contain", "HTTP_HOST = azenv.net")

	cmdline.Parse(os.Args)

	if cmdline.OptionValue("input") != "" {
		if cmdline.IsOptionSet("v") {
			log.Println(au.Bold(au.Cyan(">¤  ========================     Verbose mode On!    =====")))
			if cmdline.IsOptionSet("e") {
				log.Println(au.Bold(au.Red(">¤  ========================     Print Error!        =====")))
				menu(cmdline.OptionValue("input"), cmdline.OptionValue("threads"), cmdline.OptionValue("url"), cmdline.OptionValue("contain"), cmdline.OptionValue("timeout"), true, true)
			} else {
				menu(cmdline.OptionValue("input"), cmdline.OptionValue("threads"), cmdline.OptionValue("url"), cmdline.OptionValue("contain"), cmdline.OptionValue("timeout"), true, false)
			}
		} else {
			if cmdline.IsOptionSet("e") {
				ask := ask4confirm("\nDo you want to activate the verbose mode so that you can see the errors ?\n")
				if ask {
					fmt.Print("\n")
					log.Println(au.Bold(au.Cyan(">¤  ========================     Verbose mode On!    =====")))
					log.Println(au.Bold(au.Red(">¤  ========================     Print Error!        =====")))
					menu(cmdline.OptionValue("input"), cmdline.OptionValue("threads"), cmdline.OptionValue("url"), cmdline.OptionValue("contain"), cmdline.OptionValue("timeout"), true, true)
				} else {
					fmt.Print("\n")
					menu(cmdline.OptionValue("input"), cmdline.OptionValue("threads"), cmdline.OptionValue("url"), cmdline.OptionValue("contain"), cmdline.OptionValue("timeout"), false, false)
				}
			}
		}
		if cmdline.IsOptionSet("v") == false && cmdline.IsOptionSet("e") == false {
			menu(cmdline.OptionValue("input"), cmdline.OptionValue("threads"), cmdline.OptionValue("url"), cmdline.OptionValue("contain"), cmdline.OptionValue("timeout"), false, false)
		}

	} else {
		fmt.Println(au.Bold(au.Red("\n Error : ")), "No input list file selected. Please do \"-h\" command or \"--help\"\n          to get help.")
		pause(true)
	}
}

func ask4confirm(str string) bool {
	var s string
	fmt.Print(str, "(y/N): ")
	_, err := fmt.Scan(&s)
	if err != nil {
		panic(err)
	}
	s = strings.TrimSpace(s)
	s = strings.ToLower(s)
	if s == "y" || s == "yes" {
		return true
	}
	return false
}

func pause(exit bool) {
	if exit == true {
		fmt.Print(au.Bold("\nPress 'Enter' to exit... "))
		bufio.NewReader(os.Stdin).ReadBytes('\n')
		os.Exit(0)
	} else {
		fmt.Print(au.Bold("\nPress 'Enter' to continue... "))
		bufio.NewReader(os.Stdin).ReadBytes('\n')
	}
}
