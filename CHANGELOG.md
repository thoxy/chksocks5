# Changelog
All notable changes to this project will be documented in this file.

## v0.1.2 - 2017-11-12
### Added
* Option to print error

### Changed
* Better UI
* Format of progress bar changed

### Fixed
* Bug with regex on File load


## v0.1.1 - 2017-11-11
### Added
* Verbose mode
* Add a progress bar for normal mode