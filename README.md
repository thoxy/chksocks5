# <div style="line-height: 50px"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/4608900/business.png?width=64" alt="Drawing" style="vertical-align: middle;width: 50px;margin-right: 5px;margin-bottom:0.25em;"/> **CHKSOCKS5**</div>
This is a simple multithreaded SOCKS5 proxies checker command line tool writen in GO.

## Features :
- Open text file and  grab proxies adresses with regex.
- Multithreaded checking SOCKS5 proxies adresses.
- Website used to check proxies can be changed.
- You can change timeout of proxy checker.

## Installation :

Linux :
```bash
export $PATH="$GOPATH/bin"
go get gitlab.com/thoxy/chksocks5
```

Windows :
```bat
set PATH=%GOPATH%\bin;%PATH%
go get gitlab.com/thoxy/chksocks5
```
## How to use it :

Help :
```bash
./chksocks5 -h
```

Minimum needed :
```bash
./chksocks5 -i proxy.txt
```
You can customise options like this xD : 
```bash
./chksocks5 -i proxy.txt -x 200 -u "https://api-global.netflix.com/account/auth" -c "Only POST is allowed" -t 15
```